from urllib.error import HTTPError

from bs4 import BeautifulSoup
import urllib.request
import threading


class NYTimesGetBody(threading.Thread):
    url = None
    result = None

    def __init__(self, url):
        threading.Thread.__init__(self)
        self.url = url

    def run(self):
        self.result = get_body(self.url)

    def get_result(self):
        return self.result


def get_body(url):
    try:
        html = urllib.request.urlopen(urllib.request.Request(url)).read()
        soup = BeautifulSoup(html, 'html.parser')
        for scripts in soup(['head', 'title', 'script', 'style', '[document]', 'a', 'button', 'nav']):
            scripts.extract()
        soup.body.find('div', attrs={'style': 'display:none;'}).extract()
        text = None
        for soups in soup.body.findAll('p', attrs={'class', 'story-body-text story-content'}):
            if text is None:
                text = soups.extract()
                text = text.getText()
            else:
                text_i = soups.extract()
                text += text_i.getText()
        return text
    except HTTPError:
        # print('error!')
        return ''


if __name__ == '__main__':
    get_body('https://www.yahoo.com')
