import requests
# import logging
import os
from datetime import datetime
import pickle
import json
from url_getter_bing import UrlGetterThread


class DataGetter():
    access_key = None

    def __init__(self):
        if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
            # logging.warning('logs dir doesn\'t exist! Creating data dir')
            os.mkdir(os.path.join(os.getcwd(), 'logs'))
            # logging.info('Created logs dir')

        # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()),
        #                     level=logging.DEBUG)

        self.access_key = pickle.load(open('keys/bing_keys.pkl', 'rb'))

    def get_data(self, topic, count=None):
        headers = {'Ocp-Apim-Subscription-Key': self.access_key}
        r = requests.get('https://api.cognitive.microsoft.com/bing/v5.0/news/search?q=' + topic + ' NOT site:nytimes.com AND NOT site:theguardian.com', headers=headers)
        r_map = json.loads(r.text, encoding='utf-8')
        print(r_map)

        urls = [r_i['url'] for r_i in r_map['value'][:20]]
        print(urls)
        threads = [UrlGetterThread(url_i) for url_i in urls]
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        results = [{'url': urls[i], 'text': thread.get_results()} for i, thread in enumerate(threads) if
                   thread.get_results() is not None]
        print(len(results))
        return results


if __name__ == '__main__':
    data_getter = DataGetter()
    data_getter.get_data('Toronto Raptors')
