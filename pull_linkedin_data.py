import requests
# import logging
import os
from datetime import datetime
import pickle
import json
from id_getter_facebook import IdGetterThread


class DataGetter():
    access_key = None

    def __init__(self, api_key):
        if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
            # logging.warning('logs dir doesn\'t exist! Creating data dir')
            os.mkdir(os.path.join(os.getcwd(), 'logs'))
            # logging.info('Created logs dir')

        # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()),
        #                     level=logging.DEBUG)

        self.access_key = api_key

    def get_data(self):
        headers = {'Host': 'api.linkedin.com', 'Connection': 'Keep-Alive',
                   'Authorization': 'Bearer ' + self.access_key}
        r_comp = requests.get('https://api.linkedin.com/v1/companies?format=json&is-company-admin=true',
                              headers=headers)
        r_json = json.loads(r_comp.text, encoding='utf-8')
        if r_json['_total'] == 0:
            # print('no pages!')
            return []
        id_i = r_json['values'][0]['id']
        r = requests.get('https://api.linkedin.com/v1/companies/'+str(id_i)+'/updates?format=json&event-type=status-update',
                         headers=headers)
        # print(r.url)
        r_map = json.loads(r.text, encoding='utf-8')
        print([r_i['updateComments']for r_i in r_map['values'][:10]])
        comments = [com['comment'] for val in r_map['values'][:100] if val['updateComments']['_total'] > 0 for com in
                    val['updateComments']['values']]
        print(comments)
        return comments


if __name__ == '__main__':
    data_getter = DataGetter()
    # data_getter.get_data()
