import threading
import pickle
import nltk
import numpy as np

dictionary = np.load('data/dictionary10k1.npy')


# print('Dictionary Loaded!')


def find_features(document):
    words = nltk.word_tokenize(document)
    features = [int(w in words) for w in dictionary]

    return features


class DataClassifier(threading.Thread):
    res = None
    data = None
    model = None
    dictionary = None

    def __init__(self, data, model_name):
        threading.Thread.__init__(self)
        self.data = data
        self.model = pickle.load(open(model_name, 'rb'))
        print(model_name)

        self.data = [find_features(datum) for datum in data]

    def run(self):
        res = self.model.predict(self.data)
        # for re in res:
        #     print(re)
        # for data, re in zip(self.data, res):
        # print(data)
        # print(re)
        # res_avg = np.average(np.array(res).astype(np.float))
        # print(res_avg)
        self.res = np.array(res).astype(np.float)

    def get_result(self):
        return self.res
