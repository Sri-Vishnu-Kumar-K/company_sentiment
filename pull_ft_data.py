import requests
# import logging
import os
from datetime import datetime
import pickle
import json


class DataGetter():
    access_key = None

    def __init__(self):
        if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
            # logging.warning('logs dir doesn\'t exist! Creating data dir')
            os.mkdir(os.path.join(os.getcwd(), 'logs'))
            # logging.info('Created logs dir')

        # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()),
        #                     level=logging.DEBUG)

        self.access_key = pickle.load(open('keys/ft_keys.pkl', 'rb'))
        # logging.info('Financial Times access key loaded')

    def get_data(self, topic):
        body = {
            "queryString": topic,
            'queryContext': {
                'curations': ['ARTICLES']
            },
            'resultContext': {
                "aspects": ["title", "summary"],
                'maxResults': 10,
                "sortOrder": "DESC",
                "sortField": "initialPublishDateTime",
            },
        }
        header = {'Content-Type': 'application/json'}
        r = requests.post('https://api.ft.com/content/search/v1?apiKey=' + self.access_key, data=json.dumps(body),
                          headers=header)
        # logging.info('making call for data on ' + topic)
        print(r.status_code)
        r_map = json.loads(r.text, encoding='utf-8')
        # print(r_map['results'][0]['results'][0]['summary']['excerpt'])
        docs = [r_i['summary']['excerpt'] for r_i in r_map['results'][0]['results'][:5]]
        # logging.info('Retrieved docs of length ' + str(len(docs)))
        # print(docs)
        return docs
        # return 0

if __name__ == '__main__':
    data_getter = DataGetter()
    data_getter.get_data('icici bank')
    print('data gotten!')
