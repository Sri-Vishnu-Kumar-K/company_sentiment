# company_sentiment
### Dependencies

* numpy
* scipy
* scikit-learn
* tweepy
* beautifulsoup4
* urllib
* pickle
* uwsgi
* Flask
* requests
* twitter

### Usage
uWSGI is used to create a socket to the Flask server app. Nginx connects to this socket to communicate with the server. Nginx acts as the reverse proxy to this server stub.
 
### APIs

* /get_twitter?code=skcor_ppa_kvs&q=*insertquery*
* /get_twitter?code=skcor_ppa_kvs&q=*insertquery*&geocode=lat,long,*dist*mi (replace dist with radius in miles with lat, long as centre)
* /get_facebook?code=skcor_ppa_kvs&q=*insertquery*
* /get_guardian?code=skcor_ppa_kvs&q=*insertquery*
* /get_ft?code=skcor_ppa_kvs&q=*insertquery*
* /get_nytimes?code=skcor_ppa_kvs&q=*insertquery*
* /get_facebook_post?code=skcor_ppa_kvs&q=*insertposturl*
* /get_linkedin?code=skcor_ppa_kvs&api_key=*key from frontend*
* /get_bing?code=skcor_ppa_kvs&q=*insertquery*

code param is used to prevent unwanted API calls to the server. The code is fixed to *skcor_ppa_kvs*. q is the query param, the query can be any text that needs to be tracked.

### API Response
* /get_twitter?code=skcor_ppa_kvs&q=*insertquery* - {"twitter":(0-1), "tweets_length":number, "tweets":*[array]*}
* /get_facebook?code=skcor_ppa_kvs&q=*insertquery* - {"facebook":(0-1), "posts_length":number, "posts":*[array]*}
* /get_nytimes?code=skcor_ppa_kvs&q=*insertquery* - {"nytimes":(0-1), "documents_length":number, "documents":*[array_links]*}
* /get_guardian?code=skcor_ppa_kvs&q=*insertquery* - {"guardian":(0-1), "documents_length":number, "documents":*[array_links]*}
* /get_ft?code=skcor_ppa_kvs&q=*insertquery* - {"ft":(0-1), "documents_length":number, "documents":*[array]*}
* /get_facebook_post?code=skcor_ppa_kvs&q=*insertquery* - {"facebook_post":(0-1), "comments_length":number, "comments":*[array]*}
* /get_linkedin?code=skcor_ppa_kvs&api_key=*key from frontend* - {"linkedin":(0-1), "comments_length":number, "comments":*[array]*}
* /get_bing?code=skcor_ppa_kvs&q=*insertquery* - {"bing":(0-1), "documents_length":number, "documents":*[array_links]*}
All API calls are made to http://45.79.7.27


### Scripts

* data_classifier.py - Driver for the classification thread.
* data_collection_utils.py - Collects tweets based on emotion.
* html_parser_guardian.py - Parse the HTML retrieved from guardian, returns parsed html doc.
* html_parser_nytimes.py - Parse the HTML retrieved from nytimes, returns parsed html doc.
* id_getter_facebook.py - Gets FB IDs based on query and retrieves data from it. 
* pull_facebook_data.py - FB data driver.
* pull_facebook_post.py - FB post driver.
* pull_ft_data.py - FT data driver. (Rate Limited)
* pull_guardian_data.py - Guardian data driver.
* pull_bing_data.py - Bing data driver.
* pull_nytimes_data.py - NYTimes data driver. (Rate Limited)
* pull_twitter_data.py - Twitter data driver. (Rate Limited)
* pull_linkedin_data.py - Linkedin data driver. (Rate Limited)
* server_app.ini - Setup for uWSGI server.
* server_app.py - Driver for the Flask server.
* train_main.py - Driver file for training.
* tweet_data_cleaner.py - Clean the tweets.
* url_getter_bing.py - Retrieve and clean the html.
* wsgi.py - Driver to setup uWSGI
