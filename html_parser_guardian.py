from urllib.error import HTTPError
import threading
from bs4 import BeautifulSoup
import urllib.request


class GuardianGetBody(threading.Thread):
    url = None
    result = None

    def __init__(self, url):
        threading.Thread.__init__(self)
        self.url = url

    def run(self):
        self.result = get_body(self.url)

    def get_result(self):
        return self.result


def get_body(url):
    try:
        html = urllib.request.urlopen(urllib.request.Request(url)).read()
        soup = BeautifulSoup(html, 'html.parser')
        for scripts in soup(['head', 'title', 'script', 'style', '[document]', 'button', 'nav']):
            scripts.extract()

        if soup.body.find('div', attrs={'style': 'display:none;'}) is not None:
            soup.body.find('div', attrs={'style': 'display:none;'}).extract()

        if soup.body.find('p', attrs={'class': 'content__dateline'}) is not None:
            soup.body.find('p', attrs={'class': 'content__dateline'}).extract()

        if soup.body.find('a', attrs={'rel': 'author'}) is not None:
            soup.body.find('a', attrs={'rel': 'author'}).extract()

        if soup.body.find('span', attrs={'class': 'block-time__absolute'}) is not None:
            soup.body.find('span', attrs={'class': 'block-time__absolute'}).extract()

        text = None
        for soups in soup.body.findAll('p')[1:]:
            if text is None:
                text = soups.extract()
                text = text.getText()
            else:
                text_i = soups.extract()
                text += text_i.getText()
        # print(text)
        while '\n\n' in text:
            text = text.replace('\n\n', '\n')
        while '\n' in text:
            text = text.replace('\n', '. ')

        # print(text)
        return text
    except HTTPError:
        # print('error!')
        return ''


if __name__ == '__main__':
    get_body('https://www.theguardian.com/business/2017/jul/03/sports-direct-founder-pubs-mike-ashley')
