import time
from tweepy.streaming import StreamListener
from tweepy import Stream
from tweepy import OAuthHandler
import json
import os
import logging
from datetime import datetime
import pickle
import argparse


class StreamGetter(StreamListener):
    mood = None
    tweets_count = 0
    tweets_limit = None

    def on_connect(self):
        print('connection complete')
        logging.info('Connection complete')

    def on_data(self, raw_data):
        tweet_json = json.loads(raw_data)
        # print(type(tweet_json['text']))
        try:
            tweet = str(tweet_json['text']).encode('utf-8')
            # print(type(tweet))
            if (not tweet_json['retweeted']) and (str('RT').encode('utf-8') not in tweet):
                tweet = tweet.replace(str('\n').encode('utf-8'), str('.').encode('utf-8'))
                self.tweets_count += 1
                if self.tweets_count > self.tweets_limit:
                    return False
                f = open('data/tweet_data_' + self.get_mood() + '.txt', 'ab')
                f.write(tweet + str('\n').encode('utf-8'))
                f.close()
        except KeyError:
            return True
        return True

    def on_error(self, status_code):
        if status_code == 420:
            logging.warning('Out of limit error, waiting 900s!')
            print('Out of limit error, waiting 900s!')
            time.sleep(900)

    def set_mood(self, mood):
        self.mood = mood

    def get_mood(self):
        return self.mood

    def set_limit(self, limit):
        self.tweets_limit = limit

    def get_limit(self):
        return self.tweets_limit


def setup_project_auth():
    if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
        logging.warning('logs dir doesn\'t exist! Creating data dir')
        os.mkdir(os.path.join(os.getcwd(), 'logs'))
        logging.info('Created logs dir')

    logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()), level=logging.DEBUG)
    keys = pickle.load(open('keys/twitter_keys.pkl', 'rb'))
    ckey = keys[0]
    csecret = keys[1]
    akey = keys[2]
    asecret = keys[3]
    logging.info('ckey: ' + ckey)
    logging.info('csecret: ' + csecret)
    logging.info('akey: ' + akey)
    logging.info('asecret: ' + asecret)
    auth = OAuthHandler(ckey, csecret)
    auth.set_access_token(akey, asecret)

    if not os.path.exists(os.path.join(os.getcwd(), 'data')):
        logging.warning('data dir doesn\'t exist! Creating data dir')
        os.mkdir(os.path.join(os.getcwd(), 'data'))
        logging.info('Created data dir')

    return auth


def get_data(mood, limit):
    auth = setup_project_auth()
    stream_getter = StreamGetter()
    stream_getter.set_mood(mood)
    stream_getter.set_limit(limit)
    twitter_stream = Stream(auth, stream_getter)
    print('stream setup complete')
    if mood == 'happy':
        twitter_stream.filter(
            track=[u"😀", u"😁", u"🙂", u"😊", u"🙂", u"😍", u"😘", u"😊", u"😇", u"🙂", u"🙃", u"😉"],
            languages=["en"])
    else:
        twitter_stream.filter(
            track=[u"😤", u"😠", u"😡", u"😶", u"😐", u"😑", u"😒", u"😞", u"😔", u"😟", u"😕", u"🙁", u"😣",
                   u"😖", u"😫", u"😩"], languages=["en"])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parser for control of ip')
    parser.add_argument('-m', action='store', dest='mood')
    parser.add_argument('-l', action='store', dest='limit', type=int)
    parser_results = parser.parse_args()
    get_data(parser_results.mood, parser_results.limit)
