import threading
import requests
# import logging
import json


class IdGetterThread(threading.Thread):
    id = None
    access_key = None
    results = None

    def __init__(self, id_i, access_key):
        threading.Thread.__init__(self)
        self.id = id_i
        self.access_key = access_key

    def run(self):
        # logging.info('Pinging id: ' + self.id)
        r = requests.get(
            'https://graph.facebook.com/' + self.id + '/posts?fields=message,comments.limit(100){message}&access_token=' + self.access_key)
        # print(r.text)
        r_map = json.loads(r.text, encoding='utf-8')
        if len(r_map['data']) != 0:
            # print(r_map['data'][0]['message'])
            results = []
            for r_data in r_map['data'][:4]:
                try:
                    for comment in r_data['comments']['data']:
                        results.append(comment['message'])
                except KeyError:
                    continue
            self.results = results
        else:
            self.results = None

    def get_results(self):
        return self.results
