import requests
# import logging
import os
from datetime import datetime
import pickle
import json
from id_getter_facebook import IdGetterThread


class DataGetter():
    access_key = None

    def __init__(self):
        if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
            # logging.warning('logs dir doesn\'t exist! Creating data dir')
            os.mkdir(os.path.join(os.getcwd(), 'logs'))
            # logging.info('Created logs dir')

        # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()),
        #                     level=logging.DEBUG)

        self.access_key = pickle.load(open('keys/facebook_keys.pkl', 'rb'))
        # logging.info('Loaded FB Access Key!')

    def get_data(self, topic):
        r = requests.get(
            'https://graph.facebook.com/v2.9/search?q=' + topic + '&type=page&access_token=' + self.access_key)
        r_map = json.loads(r.text, encoding='utf-8')
        ids = [msg['id'] for msg in r_map['data'][:2]]
        # print(ids)
        threads = [IdGetterThread(id_i, self.access_key) for id_i in ids]
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        results = [thread.get_results() for thread in threads]
        # print(results)
        return results
        # return 0

if __name__ == '__main__':
    data_getter = DataGetter()
    data_getter.get_data('dcb bank')
