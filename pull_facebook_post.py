import requests
# import logging
import os
from datetime import datetime
import pickle
import json
from id_getter_facebook import IdGetterThread


class DataGetter():
    access_key = None

    def __init__(self):
        if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
            # logging.warning('logs dir doesn\'t exist! Creating data dir')
            os.mkdir(os.path.join(os.getcwd(), 'logs'))
            # logging.info('Created logs dir')

        # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()),
        #                     level=logging.DEBUG)

        self.access_key = pickle.load(open('keys/facebook_keys.pkl', 'rb'))
        # logging.info('Loaded FB Access Key!')

    def get_data(self, url):
        url = url.split('?')[0]
        # print(url)
        if url.endswith('/'):
            post_id = url.split('/')[-2]
            topic = url.split('/')[-4]
            # print(post_id)
            # print(topic)
        else:
            post_id = url.split('/')[-1]
            topic = url.split('/')[-3]
            # print(post_id)
            # print(topic)
        try:
            r = requests.get(
                'https://graph.facebook.com/v2.9/search?q=' + topic + '&type=page&access_token=' + self.access_key)
            r_map = json.loads(r.text, encoding='utf-8')
            id = r_map['data'][0]['id']
            r = requests.get('https://graph.facebook.com/' + id + '_' + post_id + '?fields=comments.limit(100){message}&access_token=' + self.access_key)
            r_map = json.loads(r.text, encoding='utf-8')
            data = r_map['comments']['data']
            results = [datum['message'] for datum in data]
            # print(len(results))
            return results
        except BaseException as e:
            return 0

if __name__ == '__main__':
    data_getter = DataGetter()
    data_getter.get_data('https://www.facebook.com/manchesterunited/posts/10154949284807746?notif_ref=201290192')
