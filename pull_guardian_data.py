import requests
# import logging
import os
from datetime import datetime
import pickle
import json
from html_parser_guardian import get_body
from html_parser_guardian import GuardianGetBody


class DataGetter():
    access_key = None

    def __init__(self):
        if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
            # logging.warning('logs dir doesn\'t exist! Creating data dir')
            os.mkdir(os.path.join(os.getcwd(), 'logs'))
            # logging.info('Created logs dir')

        # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()),
        #                     level=logging.DEBUG)

        self.access_key = pickle.load(open('keys/guardian_keys.pkl', 'rb'))
        # logging.info('Guardian access key loaded')

    def get_data(self, topic):
        r = requests.get(
            'https://content.guardianapis.com/search?api-key=' + self.access_key + '&q=' + topic + '&from-date=2016-01-01&order-by=newest')
        # logging.info('making call for data on ' + topic)
        r_map = json.loads(r.text, encoding='utf-8')
        # print(r_map['response']['docs'][0]['lead_paragraph'])
        threads = [GuardianGetBody(doc['webUrl']) for doc in r_map['response']['results'][:3]]
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        for i, doc in enumerate(r_map['response']['results'][:3]):
            # logging.info('Pinging the following page ' + doc['webUrl'] + ' index: ' + str(i))
            doc['html'] = threads[i].get_result()
        docs = [r_i for r_i in r_map['response']['results'][:3]]
        # logging.info('Retrieved docs of length ' + str(len(docs)))
        # print(docs)
        # return 0
        return docs

if __name__ == '__main__':
    data_getter = DataGetter()
    data_getter.get_data('football')
    print('data gotten!')
