import nltk
import numpy as np
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
# import train_thread
import pickle
from sklearn.metrics import confusion_matrix


# nltk.download()

def prepare_dataset(files):
    dataset = []
    allowed_types = ['J', 'R']
    prev_len = 0
    all_words = []
    for i, file in enumerate(files):
        print(file)
        docs = np.load(file)
        for j, doc in enumerate(docs[:30000]):
            if j % 1000 == 0:
                print(j)
            doc = doc.decode('utf-8')
            doc = doc.replace('@', '')
            try:
                words = nltk.word_tokenize(doc)
                pos = nltk.pos_tag(words)
                for w in pos:
                    if w[1][0] in allowed_types:
                        all_words.append(w[0].lower())
                curr_len = len(all_words)
                if prev_len < curr_len:
                    if i == 0:
                        dataset.append((doc, 0))
                    else:
                        dataset.append((doc, 1))
                prev_len = curr_len
            except BaseException as e:
                print(e)

    dataset = np.array(dataset)
    # print(dataset)
    np.save('data/dataset60', dataset)
    print('Saved dataset!')
    np.save('data/dictionary10k', np.array(all_words))
    print('Saved dictionary')
    all_words = nltk.FreqDist(all_words)
    word_features = list(all_words.keys())[:10000]
    print(word_features[:10])
    np.save('data/dictionary10k1', np.array(word_features))
    print('Saved 10k dictionary')


dictionary = None


def find_features(document, i):
    if i % 1000 == 0:
        print(i)
    words = nltk.word_tokenize(document)
    features = [int(w in words) for w in dictionary]

    return features


def create_xy():
    print(dictionary[:10])
    dataset = np.load('data/dataset60.npy')
    x = [find_features(data, i) for i, (data, emotion) in enumerate(dataset)]
    y = [emotion for data, emotion in dataset]
    # print(x)
    # print(y)
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.2, random_state=41)
    pickle.dump(x_train, open('data/x_train5k6.pkl', 'wb'))
    pickle.dump(x_test, open('data/x_test5k6.pkl', 'wb'))
    pickle.dump(y_train, open('data/y_train5k6.pkl', 'wb'))
    pickle.dump(y_test, open('data/y_test5k6.pkl', 'wb'))
    # np.save('data/x_train5k', x_train)
    # np.save('data/x_test5k', x_test)
    # np.save('data/y_train5k', y_train)
    # np.save('data/y_test5k', y_test)
    # print('X&Y generated! %d %d %d %d' % (x_train.shape, x_test.shape, y_train.shape, y_test.shape))


def load_xy_train():
    x_train = pickle.load(open('data/x_train5k6.pkl', 'rb'))
    y_train = pickle.load(open('data/y_train5k6.pkl', 'rb'))

    return x_train, y_train


def load_xy_test():
    x_test = pickle.load(open('data/x_test5k6.pkl', 'rb'))
    y_test = pickle.load(open('data/y_test5k6.pkl', 'rb'))

    return x_test, y_test


def load_xy():
    x_train = pickle.load(open('data/x_train5k6.pkl', 'rb'))
    x_test = pickle.load(open('data/x_test5k6.pkl', 'rb'))
    y_train = pickle.load(open('data/y_train5k6.pkl', 'rb'))
    y_test = pickle.load(open('data/y_test5k6.pkl', 'rb'))
    return x_train, x_test, y_train, y_test


def get_rows(chunkrows):
    x_train, y_train = load_xy_train()
    return x_train[chunkrows[0]:chunkrows[1]], y_train[chunkrows[0]:chunkrows[1]]


def iter_minibatch(chunk_size):
    chunk_start_marker = 0
    i = 0
    while i < 2:
        chunkrows = [chunk_start_marker, chunk_start_marker + chunk_size]
        X_chunk, y_chunk = get_rows(chunkrows)
        yield X_chunk, y_chunk
        chunk_start_marker += chunk_size
        i += 1


def train():
    for i in range(8):
        if i == 0:
            # x_train, y_train = load_xy_train()
            # MNB_classifier = MultinomialNB()
            # for x_train, y_train in iter_minibatch(chunk_size=20000):
            #     MNB_classifier.partial_fit(x_train, y_train, classes=[0, 1])
            # del x_train
            # del y_train
            # x_test, y_test = load_xy_test()
            # print("MNB_classifier")
            # print(confusion_matrix(y_test, MNB_classifier.predict(x_test)))
            # count = 0
            # y_pred = MNB_classifier.predict(x_test)
            # for i in range(len(y_pred)):
            #     try:
            #         if y_test[i] == int(y_pred[i]):
            #             count += 1
            #     except BaseException as e:
            #         continue
            # print(float(count)/float(len(y_pred)))
            # save_classifier = open("models/MNB_classifier5k.pkl", "wb")
            # pickle.dump(MNB_classifier, save_classifier)
            # save_classifier.close()
            print("Done")
        elif i == 1:
            # x_train, y_train = load_xy_train()
            # BernoulliNB_classifier = BernoulliNB()
            # BernoulliNB_classifier.fit(x_train, y_train)
            # del x_train
            # del y_train
            # x_test, y_test = load_xy_test()
            # print("BernoulliNB_classifier")
            # print(confusion_matrix(y_test, BernoulliNB_classifier.predict(x_test)))
            # save_classifier = open("models/BernoulliNB_classifier5k.pkl", "wb")
            # pickle.dump(BernoulliNB_classifier, save_classifier)
            # save_classifier.close()
            print("Done")
        elif i == 2:
            x_train, y_train = load_xy_train()
            LogisticRegression_classifier = LogisticRegression()
            LogisticRegression_classifier.fit(x_train, y_train)
            del x_train
            del y_train
            x_test, y_test = load_xy_test()
            print("Logistic Regression")
            print(confusion_matrix(y_test, LogisticRegression_classifier.predict(x_test)))
            save_classifier = open("models/LogisticRegression_classifier5k.pkl", "wb")
            pickle.dump(LogisticRegression_classifier, save_classifier)
            save_classifier.close()
            print("Done")
        elif i == 3:
            x_train, y_train = load_xy_train()
            LinearSVC_classifier = LinearSVC()
            LinearSVC_classifier.fit(x_train, y_train)
            del x_train
            del y_train
            x_test, y_test = load_xy_test()
            print("Linear SVC")
            print(confusion_matrix(y_test, LinearSVC_classifier.predict(x_test)))
            save_classifier = open("models/LinearSVC_classifier5k.pkl", "wb")
            pickle.dump(LinearSVC_classifier, save_classifier)
            save_classifier.close()
            print("Done")
        elif i == 4:
            x_train, y_train = load_xy_train()
            SGDC_classifier = SGDClassifier()
            SGDC_classifier.fit(x_train, y_train)
            del x_train
            del y_train
            x_test, y_test = load_xy_test()
            print("SGDC classifier")
            print(confusion_matrix(y_test, SGDC_classifier.predict(x_test)))
            save_classifier = open("models/SGDC_classifier5k.pkl", "wb")
            pickle.dump(SGDC_classifier, save_classifier)
            save_classifier.close()
            print("Done")
        elif i == 5:
            x_train, y_train = load_xy_train()
            Ada_classifier = AdaBoostClassifier()
            Ada_classifier.fit(x_train, y_train)
            del x_train
            del y_train
            x_test, y_test = load_xy_test()
            print("AdaBoost")
            print(confusion_matrix(y_test, Ada_classifier.predict(x_test)))
            save_classifier = open("models/Ada_classifier5k.pkl", "wb")
            pickle.dump(Ada_classifier, save_classifier)
            save_classifier.close()
            print("Done")
        elif i == 6:
            x_train, y_train = load_xy_train()
            DT_classifier = DecisionTreeClassifier()
            DT_classifier.fit(x_train, y_train)
            del x_train
            del y_train
            x_test, y_test = load_xy_test()
            print("Decision Tree")
            print(confusion_matrix(y_test, DT_classifier.predict(x_test)))
            save_classifier = open("models/DT_classifier5k.pkl", "wb")
            pickle.dump(DT_classifier, save_classifier)
            save_classifier.close()
            print("Done")
        elif i == 7:
            x_train, y_train = load_xy_train()
            RF_classifier = RandomForestClassifier()
            RF_classifier.fit(x_train, y_train)
            del x_train
            del y_train
            x_test, y_test = load_xy_test()
            print("Random Forest")
            print(confusion_matrix(y_test, RF_classifier.predict(x_test)))
            save_classifier = open("models/RF_classifier5k.pkl", "wb")
            pickle.dump(RF_classifier, save_classifier)
            save_classifier.close()
            print("Done")
    return


if __name__ == '__main__':
    # prepare_dataset(['data/cleaned_tweet_data_happy.npy', 'data/cleaned_tweet_data_sad.npy'])
    # dictionary = np.load('data/dictionary10k1.npy')
    # create_xy()
    train()
