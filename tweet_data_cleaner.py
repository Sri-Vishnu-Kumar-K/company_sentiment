import re
import numpy as np


def clean_tweet(doc):
    emoji_array = ["😤", "😠", "😡", "😶", "😐", "😑", "😒", "😞", "😔", "😟", "😕", "🙁", "😣", "😖",
                   "😫", "😩", "😀", "😁", "🙂", "😊", "🙂", "😍", "😘", "😊", "😇", "🙂", "🙃", "😉"]
    for emoji in emoji_array:
        doc = doc.replace(emoji.encode('utf-8'), str('').encode('utf-8'))
    doc = doc.replace('\n'.encode('utf-8'), ''.encode('utf-8'))
    doc = doc.replace('#'.encode('utf-8'), ''.encode('utf-8'))
    doc = re.sub(r'@[A-Za-z0-9_]+\s'.encode('utf-8'), ''.encode('utf-8'), doc)

    return doc


def clean_document(file):
    docs = open('data/' + file + '.txt', mode='rb').readlines()
    # print(docs[:10])
    cleaned_docs = [clean_tweet(doc) for doc in docs]
    cleaned_docs = np.array(cleaned_docs)
    # print(cleaned_docs)
    np.save('data/cleaned_' + file, cleaned_docs)
    print('The size is ' + str(cleaned_docs.shape))
    print('All cleaning done for ' + file)


if __name__ == '__main__':
    clean_document('tweet_data_happy')
    clean_document('tweet_data_sad')
