import requests
import os
import pickle
import json


def setup_linkedin():
    params = {'grant_type': 'authorization_code',
              'code': 'AQQM9TwGYJX79iq6G6b8XtV9JdP_w2XJXhu8FjFc6dkzJW888mr0WWWZsvwvRURg1IcIGqlHyXU-4wskwYN5ZfOFbTCmJSECifApw_LSybvgK0k5nyY',
              'redirect_uri': 'http://www.albertatechworks.com', 'client_id': '81qo8s6f9p2794',
              'client_secret': '8Mfb8cYyFOv1cGVm'}
    headers = {'Host': 'www.linkedin.com', 'Content-Type': 'application/x-www-form-urlencoded'}
    r = requests.post('https://www.linkedin.com/oauth/v2/accessToken', params=params, headers=headers)
    r_json = json.loads(r.text)
    print(r_json)
    pickle.dump(r_json['access_token'], open('keys/linkedin_keys.pkl', 'wb'))


if __name__ == '__main__':
    setup_linkedin()
