import threading
import urllib
from urllib.error import HTTPError
from bs4 import BeautifulSoup


class UrlGetterThread(threading.Thread):
    url = None
    results = None

    def __init__(self, url):
        threading.Thread.__init__(self)
        self.url = url

    def run(self):
        # logging.info('Pinging id: ' + self.id)
        self.results = get_body(self.url)

    def get_results(self):
        return self.results


def get_body(url):
    try:
        html = urllib.request.urlopen(urllib.request.Request(url)).read()
        soup = BeautifulSoup(html, 'html.parser')
        for scripts in soup(['head', 'title', 'script', 'style', '[document]', 'a', 'button', 'nav']):
            scripts.extract()
        text = None
        for soups in soup.body.findAll('p'):
            if text is None:
                text = soups.extract()
                text = text.getText()
            else:
                text_i = soups.extract()
                text += text_i.getText()
        return text
    except BaseException as e:
        # print(e)
        return None
