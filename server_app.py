from flask import Flask
from flask import request
import re
import pull_bing_data
import pull_linkedin_data
import pull_twitter_data
import pull_facebook_data
import pull_nytimes_data
import pull_ft_data
import pull_guardian_data
import json
import pull_facebook_post
# import logging
from tweet_data_cleaner import clean_tweet
from data_classifier import DataClassifier
import numpy as np
from nltk.corpus import stopwords
import operator

app = Flask(__name__)
models = ['models/LogisticRegression_classifier5k.pkl', 'models/LinearSVC_classifier5k.pkl',
          'models/MNB_classifier5k.pkl']


def imp_words(sentences):
    try:
        count = {}
        stop = set(stopwords.words('english'))
        stop.update(['rt', 'RT', 'rT', 'Rt', "😤", "😠", "😡", "😶", "😐", "😑", "😒", "😞", "😔", "😟", "😕", "🙁", "😣", "😖", "😫", "😩", "😀", "😁", "🙂", "😊", "🙂", "😍", "😘", "😊", "😇", "🙂", "🙃", "😉"])
        emoji_pattern = re.compile("["u"\U0001F600-\U0001F64F"  # emoticons
                                   u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                                   u"\U0001F680-\U0001F6FF"  # transport & map symbols
                                   u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                                   "]+", flags=re.UNICODE)
        for sentence in sentences:
            words = sentence.split()
            words = zip(words[:-1], words[1:])
            for word1, word2 in words:
                if word1.lower() not in stop and word2.lower() not in stop and len(re.findall(emoji_pattern, word1.lower())) == 0 and len(re.findall(emoji_pattern, word2.lower())) == 0:
                    bigram = word1.lower() + ' ' + word2.lower()
                    if bigram not in count.keys():
                        count[bigram] = 1
                    else:
                        count[bigram] += 1
        if len(count) > 0:
            count = sorted(count.items(), key=operator.itemgetter(1), reverse=True)
            words = [i for i, v in count[:30]]
            return words
        else:
            return []
    except BaseException as e:
        print(e)


@app.route('/get_twitter', methods=['GET'])
def get_twitter():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                if request.args.get('geocode') is not None:
                    tweet_array = pull_twitter_data.get_data(request.args.get('q').replace(' ', ''),
                                                             request.args.get('geocode'))
                else:
                    tweet_array = pull_twitter_data.get_data(request.args.get('q').replace(' ', ''))
                tweet = [clean_tweet(twt).decode('utf-8') for twt in tweet_array]
                if len(tweet) > 0:
                    threads = [DataClassifier(tweet, model) for model in models]
                    for thread in threads:
                        thread.start()
                    for thread in threads:
                        thread.join()
                    if request.args.get('view') == 'list':
                        res = np.array([thread.get_result() for thread in threads])
                        # print(res.shape)
                        res_3_avg = [np.average(res[:, i]) for i in range(res.shape[1])]
                        tweet_neg = [tweet_i1 for tweet_i1, res_i1 in zip(tweet, res_3_avg) if res_i1 > .5]
                        neg_words = imp_words(tweet_neg)
                        ret_json = json.dumps(
                            {'twitter': [{'tweet': tweet_i, 'res': res_i} for tweet_i, res_i in zip(tweet, res_3_avg)],
                             'tweets_length': len(tweet), 'imp_words': neg_words})
                        return ret_json
                    res_avg = np.average(np.array([np.average(thread.get_result()) for thread in threads]))
                    ret_json = json.dumps({'twitter': res_avg, 'tweets_length': len(tweet), 'tweets': tweet})
                else:
                    ret_json = json.dumps({'twitter': .5, 'tweets_length': len(tweet), 'tweets': tweet})
                return ret_json
            except BaseException as e:
                print(e)
                return json.dumps({'twitter': -1})


@app.route('/get_facebook', methods=['GET'])
def get_facebook():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                facebook_data_getter = pull_facebook_data.DataGetter()
                res = facebook_data_getter.get_data(request.args.get('q'))
                strings = [res_j for res_i in res if res_i is not None for res_j in res_i]
                threads = [DataClassifier(strings, model) for model in models]
                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                if request.args.get('view') == 'list':
                    res = np.array([thread.get_result() for thread in threads])
                    # print(res.shape)
                    res_3_avg = [np.average(res[:, i]) for i in range(res.shape[1])]
                    post_neg = [post_i for post_i, res_i in zip(strings, res_3_avg) if res_i > 0.5]
                    neg_words = imp_words(post_neg)
                    # print(len(res_3_avg))
                    ret_json = json.dumps(
                        {'facebook': [{'post': post_i, 'res': res_i} for post_i, res_i in zip(strings, res_3_avg)],
                         'posts_length': len(strings), 'imp_words': neg_words})
                    return ret_json
                res_avg = np.average(np.array([np.average(thread.get_result()) for thread in threads]))
                ret_json = json.dumps({'facebook': res_avg, 'posts_length': len(strings), 'posts': strings})
                return ret_json
            except BaseException as e:
                print(e)
                return json.dumps({'facebook': -1})


@app.route('/get_nytimes', methods=['GET'])
def get_nytimes():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                nytimes_data_getter = pull_nytimes_data.DataGetter()
                res = nytimes_data_getter.get_data(request.args.get('q'))
                strings = [doc['html'] if doc['html'] != '' else doc['lead_paragraph'] for doc in res]
                links = [doc['web_url'] for doc in res]
                threads = [DataClassifier(strings, model) for model in models]
                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                if request.args.get('view') == 'list':
                    res = np.array([thread.get_result() for thread in threads])
                    # print(res.shape)
                    res_3_avg = [np.average(res[:, i]) for i in range(res.shape[1])]
                    # print(len(res_3_avg))
                    ret_json = json.dumps(
                        {'nytimes': [{'link': link_i, 'res': res_i} for link_i, res_i in zip(links, res_3_avg)],
                         'documents_length': len(links)})
                    return ret_json
                res_avg = np.average(np.array([np.average(thread.get_result()) for thread in threads]))
                ret_json = json.dumps({'nytimes': res_avg, 'documents_length': len(strings), 'documents': links})
                return ret_json
            except BaseException as e:
                return json.dumps({'nytimes': -1})


@app.route('/get_ft', methods=['GET'])
def get_ft():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                ft_data_getter = pull_ft_data.DataGetter()
                res = ft_data_getter.get_data(request.args.get('q'))
                threads = [DataClassifier(res, model) for model in models]
                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                if request.args.get('view') == 'list':
                    res_t = np.array([thread.get_result() for thread in threads])
                    # print(res.shape)
                    res_3_avg = [np.average(res_t[:, i]) for i in range(res_t.shape[1])]
                    # print(len(res_3_avg))
                    ret_json = json.dumps(
                        {'ft': [{'document': doc_i, 'res': res_i} for doc_i, res_i in zip(res, res_3_avg)],
                         'documents_length': len(res)})
                    return ret_json
                res_avg = np.average(np.array([np.average(thread.get_result()) for thread in threads]))
                ret_json = json.dumps({'ft': res_avg, 'documents_length': len(res), 'documents': res})
                return ret_json
            except BaseException as e:
                return json.dumps({'ft': -1})


@app.route('/get_guardian', methods=['GET'])
def get_guardian():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                guardian_data_getter = pull_guardian_data.DataGetter()
                res = guardian_data_getter.get_data(request.args.get('q'))
                strings = [doc['html'] if doc['html'] != '' else doc['webTitle'] for doc in res]
                links = [doc['webUrl'] for doc in res]
                threads = [DataClassifier(strings, model) for model in models]
                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                if request.args.get('view') == 'list':
                    res = np.array([thread.get_result() for thread in threads])
                    # print(res.shape)
                    res_3_avg = [np.average(res[:, i]) for i in range(res.shape[1])]
                    # print(len(res_3_avg))
                    ret_json = json.dumps(
                        {'guardian': [{'tweet': link_i, 'res': res_i} for link_i, res_i in zip(links, res_3_avg)],
                         'documents_length': len(strings)})
                    return ret_json
                res_avg = np.average(np.array([np.average(thread.get_result()) for thread in threads]))
                ret_json = json.dumps({'guardian': res_avg, 'documents_length': len(strings), 'documents': links})
                return ret_json
            except BaseException as e:
                return json.dumps({'guardian': -1})


@app.route('/get_facebook_post', methods=['GET'])
def get_facebook_post():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                facebook_post_getter = pull_facebook_post.DataGetter()
                res = facebook_post_getter.get_data(request.args.get('q'))
                if res == 0:
                    return json.dumps({'facebook_post': 0.5, 'comments_length': len(res), 'comments': res})
                threads = [DataClassifier(res, model) for model in models]
                for thread in threads:
                    thread.start()
                for thread in threads:
                    thread.join()
                res_avg = np.average(np.array([thread.get_result() for thread in threads]))
                ret_json = json.dumps({'facebook_post': res_avg, 'comments_length': len(res), 'comments': res})
                return ret_json
            except BaseException as e:
                return json.dumps({'facebook_post': -1})


@app.route('/get_linkedin', methods=['GET'])
def get_linkedin():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                # print(request.args.get('api_key'))
                linkedin_data_getter = pull_linkedin_data.DataGetter(str(request.args.get('api_key')))
                # print('hi')
                res = linkedin_data_getter.get_data()
                # print(res)
                if len(res) > 0:
                    threads = [DataClassifier(res, model) for model in models]
                    for thread in threads:
                        thread.start()
                    for thread in threads:
                        thread.join()
                    if request.args.get('view') == 'list':
                        res_t = np.array([thread.get_result() for thread in threads])
                        # print(res.shape)
                        res_3_avg = [np.average(res_t[:, i]) for i in range(res_t.shape[1])]
                        # print(len(res_3_avg))
                        post_neg = [post_i for post_i, res_i in zip(res, res_3_avg) if res_i > 0.5]
                        neg_words = imp_words(post_neg)
                        ret_json = json.dumps(
                            {'linkedin': [{'comment': comment_i, 'res': res_i} for comment_i, res_i in
                                          zip(res, res_3_avg)],
                             'comments_length': len(res), 'imp_words': neg_words})
                        return ret_json
                    res_avg = np.average(np.array([np.average(thread.get_result()) for thread in threads]))
                    # print(res_avg)
                    ret_json = json.dumps({'linkedin': res_avg, 'comments_length': len(res), 'comments': res})
                    return ret_json
                else:
                    ret_json = json.dumps({'linkedin': 0.5, 'comments_length': len(res), 'comments': res})
                    return ret_json
            except BaseException as e:
                print(e)
                return json.dumps({'linkedin': -1})


@app.route('/get_bing', methods=['GET'])
def get_bing():
    if request.method == 'GET':
        if request.args.get('code') == 'skcor_ppa_kvs':
            try:
                bing_data_getter = pull_bing_data.DataGetter()
                res = bing_data_getter.get_data(request.args.get('q'))
                texts = [res_i['text'] for res_i in res]
                urls = [res_i['url'] for res_i in res]
                if len(res) > 0:
                    threads = [DataClassifier(texts, model) for model in models]
                    for thread in threads:
                        thread.start()
                    for thread in threads:
                        thread.join()
                    if request.args.get('view') == 'list':
                        res_t = np.array([thread.get_result() for thread in threads])
                        # print(res.shape)
                        res_3_avg = [np.average(res_t[:, i]) for i in range(res_t.shape[1])]
                        # print(len(res_3_avg))
                        ret_json = json.dumps(
                            {'bing': [{'document': url_i, 'res': res_i} for url_i, res_i in zip(urls, res_3_avg)],
                             'documents_length': len(urls)})
                        return ret_json
                    res_avg = np.average(np.array([np.average(thread.get_result()) for thread in threads]))
                    ret_json = json.dumps({'bing': res_avg, 'documents_length': len(res), 'documents': urls})
                    return ret_json
                else:
                    ret_json = json.dumps({'bing': 0.5, 'documents_length': len(res), 'documents': urls})
                    return ret_json
            except BaseException as e:
                print(e)
                return json.dumps({'bing': -1})


if __name__ == '__main__':
    app.run('0.0.0.0')
