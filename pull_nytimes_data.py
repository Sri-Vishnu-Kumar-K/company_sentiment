import requests
# import logging
import os
from datetime import datetime
import pickle
import json
from html_parser_nytimes import get_body
from html_parser_nytimes import NYTimesGetBody

class DataGetter():
    access_key = None

    def __init__(self):
        if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
            # logging.warning('logs dir doesn\'t exist! Creating data dir')
            os.mkdir(os.path.join(os.getcwd(), 'logs'))
            # logging.info('Created logs dir')

        # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()),
        #                     level=logging.DEBUG)

        self.access_key = pickle.load(open('keys/nytimes_keys.pkl', 'rb'))
        # logging.info('NYTimes access key loaded')

    def get_data(self, topic):
        r = requests.get(
            'https://api.nytimes.com/svc/search/v2/articlesearch.json?api-key=' + self.access_key + '&q=' + topic + '&sort=newest&fl=web_url,lead_paragraph,headline')
        # logging.info('making call for data on ' + topic)
        r_map = json.loads(r.text, encoding='utf-8')
        # print(r_map['response']['docs'][0]['lead_paragraph'])
        threads = [NYTimesGetBody(doc['web_url']) for doc in r_map['response']['docs']]
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
        for i, doc in enumerate(r_map['response']['docs']):
            # logging.info('Pinging the following page ' + doc['web_url'] + ' index: ' + str(i))
            doc['html'] = threads[i].get_result()
        docs = [r_i for r_i in r_map['response']['docs'][:3]]
        # logging.info('Retrieved docs of length ' + str(len(docs)))
        # print(docs)
        return docs
        # return 0

if __name__ == '__main__':
    data_getter = DataGetter()
    data_getter.get_data('citi bank')
    print('data gotten!')
