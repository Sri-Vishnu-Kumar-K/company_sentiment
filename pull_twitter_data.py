import time
from twitter import *
import os
# import logging
from datetime import datetime
import pickle

# from tweet_data_cleaner import clean_tweet


def setup_project_auth():
    if not os.path.exists(os.path.join(os.getcwd(), 'logs')):
        # logging.warning('logs dir doesn\'t exist! Creating data dir')
        os.mkdir(os.path.join(os.getcwd(), 'logs'))
        # logging.info('Created logs dir')

    # logging.basicConfig(filename='logs/logfile_{:%d-%m-%y_%H-%M-%S}.log'.format(datetime.now()), level=logging.DEBUG)
    keys = pickle.load(open('keys/twitter_keys.pkl', 'rb'))
    ckey = keys[0]
    csecret = keys[1]
    akey = keys[2]
    asecret = keys[3]
    # logging.info('ckey: ' + ckey)
    # logging.info('csecret: ' + csecret)
    # logging.info('akey: ' + akey)
    # logging.info('asecret: ' + asecret)
    twitter = Twitter(auth=OAuth(akey, asecret, ckey, csecret))

    return twitter


def get_data(mood, geocode=None):
    twitter_stream = setup_project_auth()
    # print('stream setup complete')
    # logging.info('Searching twitter for '+mood)
    # print('Searching twitter for ' + mood)
    if geocode is None:
        query = twitter_stream.search.tweets(q=mood, result_type='recent', lang='en', count=100)
    else:
        query = twitter_stream.search.tweets(q=mood, result_type='recent', lang='en', count=100, geocode=geocode)
    res = []
    for r_i in query['statuses']:
        try:
            if not r_i['retweeted']:
                res.append(r_i['text'].encode('utf-8'))
        except BaseException as e:
            continue
    # print(res)
    # tweet = [clean_tweet(twt).decode('utf-8') for twt in res]
    # print(tweet)
    return res
    # return 0


if __name__ == '__main__':
    get_data("dcbbank")
